"""Top level module for spamcheck service."""

from app import config

config.load()


class ValidationError(Exception):
    """An exception indicating an object failed validation checks"""
