# Set python virtual environment
export PIPENV_VENV_IN_PROJECT=1
VENV := ./.venv

SHELL = /usr/bin/env bash -eo pipefail

SPAMCHECK_PROTO_PATH := ${PWD}/api/v1
SPAMCHECK_PROTO      := ${SPAMCHECK_PROTO_PATH}/*.proto
RUBY_PROTO_PATH      := ${PWD}/ruby/spamcheck

deps: ${VENV}

${VENV}:
	pip install pipenv
	pipenv install --dev

# Generate Python protobuf files
proto:
	pipenv run python -m grpc_tools.protoc \
		--proto_path=${PWD} \
		--python_out=${PWD} \
		--grpc_python_out=${PWD} \
		${SPAMCHECK_PROTO}

# Generate Ruby protobuf files
proto_ruby:
	gem install bundler
	bundle install
	bundle exec grpc_tools_ruby_protoc \
		--proto_path=${SPAMCHECK_PROTO_PATH} \
		--ruby_out=${RUBY_PROTO_PATH} \
		--grpc_out=${RUBY_PROTO_PATH} \
		${SPAMCHECK_PROTO}

gem: proto_ruby
	bundle exec ruby _support/publish-gem

lint: ${VENV} proto
	 pipenv run pylint --rcfile=.pylintrc *.py app server

test: ${VENV} proto
	ENV=test PYTHONDONTWRITEBYTECODE=1 pipenv run coverage run -m pytest -W ignore::DeprecationWarning
	pipenv run coverage report -m

run: ${VENV} proto
	pipenv run python main.py

clean:
	rm -rf ${SPAMCHECK_PROTO_PATH}/*.py

.PHONY: proto proto_ruby gem test run clean
